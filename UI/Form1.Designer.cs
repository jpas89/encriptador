﻿namespace Encriptador
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.cifradoParaleloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cifrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cifradoSerialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cifrarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.encriptadorParaleloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.encriptadorSerialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cifradoParaleloToolStripMenuItem
            // 
            this.cifradoParaleloToolStripMenuItem.Name = "cifradoParaleloToolStripMenuItem";
            this.cifradoParaleloToolStripMenuItem.Size = new System.Drawing.Size(103, 20);
            this.cifradoParaleloToolStripMenuItem.Text = "Cifrado Paralelo";
            // 
            // cifrarToolStripMenuItem
            // 
            this.cifrarToolStripMenuItem.Name = "cifrarToolStripMenuItem";
            this.cifrarToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.cifrarToolStripMenuItem.Text = "Cifrar";
            this.cifrarToolStripMenuItem.Click += new System.EventHandler(this.cifrarToolStripMenuItem_Click);
            // 
            // cifradoSerialToolStripMenuItem
            // 
            this.cifradoSerialToolStripMenuItem.Name = "cifradoSerialToolStripMenuItem";
            this.cifradoSerialToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.cifradoSerialToolStripMenuItem.Text = "Cifrado Serial";
            // 
            // cifrarToolStripMenuItem1
            // 
            this.cifrarToolStripMenuItem1.Name = "cifrarToolStripMenuItem1";
            this.cifrarToolStripMenuItem1.Size = new System.Drawing.Size(103, 22);
            this.cifrarToolStripMenuItem1.Text = "Cifrar";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(314, 325);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // encriptadorParaleloToolStripMenuItem
            // 
            this.encriptadorParaleloToolStripMenuItem.Name = "encriptadorParaleloToolStripMenuItem";
            this.encriptadorParaleloToolStripMenuItem.Size = new System.Drawing.Size(125, 20);
            this.encriptadorParaleloToolStripMenuItem.Text = "Encriptador Paralelo";
            this.encriptadorParaleloToolStripMenuItem.Click += new System.EventHandler(this.encriptadorParaleloToolStripMenuItem_Click);
            // 
            // encriptadorSerialToolStripMenuItem
            // 
            this.encriptadorSerialToolStripMenuItem.Name = "encriptadorSerialToolStripMenuItem";
            this.encriptadorSerialToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.encriptadorSerialToolStripMenuItem.Text = "Encriptador Serial";
            this.encriptadorSerialToolStripMenuItem.Click += new System.EventHandler(this.encriptadorSerialToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.encriptadorParaleloToolStripMenuItem,
            this.encriptadorSerialToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(314, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(41, 20);
            this.toolStripMenuItem1.Text = "Salir";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(314, 349);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Location = new System.Drawing.Point(400, 200);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(330, 388);
            this.MinimumSize = new System.Drawing.Size(330, 388);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Encriptador ";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem cifradoParaleloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cifrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cifradoSerialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cifrarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem encriptadorParaleloToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem encriptadorSerialToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}

